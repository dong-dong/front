self.className = "Worker";
self.importScripts("common.js");
self.importScripts("gjvideo_protobuf.js")
self.importScripts("reconnecting-websocket.js");

function Receiver() {
    console.log("constructor Receiver");
    this.className = "Receiver";
    this.url = undefined;
    this.clientId = undefined;
    this.token = undefined;
    this.deviceId = undefined;
    this.onlyVideo = true;
    var websocket = null;
    this.createWebsocket = function () {
        var settings = {
            debug: false,
            automaticOpen: false,
            reconnectInterval: 1000,
            maxReconnectInterval: 30000,
            reconnectDecay: 1.5,
            timeoutInterval: 2000,
            maxReconnectAttempts: null,
            binaryType: 'arraybuffer'
        }
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        websocket = new ReconnectingWebSocket(this.url, undefined, settings, function (eventType, theWebsocket ,arg1, arg2, arg3) {
            // console.log(theWebsocket)
            if (eventType === 'connecting') {
                console.log('connecting');
            } else if (eventType === 'open') {
                console.log('open');
                //https://blog.csdn.net/jom_ch/article/details/78855043
                var streamInfo = new proto.gjvideo.StreamInfo();
                streamInfo.setClientid(self.receiver.clientId);
                streamInfo.setToken(self.receiver.token);
                streamInfo.setDeviceid(self.receiver.deviceId);
                streamInfo.setOnlyvideo(self.receiver.onlyVideo);
                streamInfo.setDevicestreamid(self.receiver.deviceStreamId);
                theWebsocket.send(streamInfo.serializeBinary());        
            } else if (eventType === 'message') {
                const arrayBuffer = arg1;
                const streamData = proto.gjvideo.StreamData.deserializeBinary(arrayBuffer);
                // console.log("[" + streamData.getType() + "][" + streamData.getTimestamp() + "][" + arrayBuffer.byteLength + "]");
                var objData = {
                    type: GJMessageType.ON_PACKET,
                    packetType: streamData.getType(),
                    message: streamData.getMessage(),
                    streamId: streamData.getWsstreamid(),
                    timestamp: streamData.getTimestamp(),
                    payload: streamData.getPayload()
                };
                self.postMessage(objData,[objData.payload.buffer]);
                objData = null;
            } else if (eventType === 'close') {
                console.log('close');
            } else if (eventType === 'error') {
                console.log('error');
            }
        });
        console.log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        websocket.open(false);
    };
    this.receiveStream = function (req) {

        this.url = req.data.url;
        this.clientId = req.data.clientId;
        this.token = req.data.token;
        this.deviceId = req.data.deviceId;
        this.deviceStreamId = req.data.streamId;
        this.onlyVideo = true;
        if (websocket) {
            console.log("already create websocekt");
            //TODO 切换Websocket链接
        } else {GJMessageType
            websocket = this.createWebsocket();
        }
    };
}

self.receiver = new Receiver();

self.onmessage = function (evt) {
    if (!self.receiver) {
        return;
    }
    var req = evt.data;
    console.log("receiver onmessage >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    console.log(req);
    console.log("receiver onmessage <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    switch (req.type) {
        case GJMessageType.CMD_PLAY:
            self.receiver.receiveStream(req);
            break;
        case GJMessageType.CMD_STOP:
            //TODO 关闭
            break;
        default:
            throw new Error("receiver error req.type");
    }
};