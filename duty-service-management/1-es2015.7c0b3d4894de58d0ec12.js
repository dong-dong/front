(window.webpackJsonp=window.webpackJsonp||[]).push([[1],{"1yWB":function(t,e,n){"use strict";n.d(e,"a",(function(){return h}));var i=n("8mwJ"),o=n("0OvT"),c=n("2zqZ"),r=n("Iq5M"),a=n("/UCm"),s=n("asV7"),l=n("HKMU"),u=n("ZC9W"),d=n("YHQG");function p(t,e){if(1&t&&(r.ic(0,"mat-list-option",11),r.ic(1,"div",12),r.ic(2,"div",13),r.Xc(3),r.hc(),r.ic(4,"div",14),r.Xc(5),r.hc(),r.hc(),r.dc(6,"mat-divider"),r.hc()),2&t){const t=e.$implicit,n=e.index;r.Ac("selected",t.isShow)("value",t),r.Pb(3),r.Yc(n+1),r.Pb(2),r.Yc(t.title)}}const g=function(){return{suppressScrollX:!0,updateOnRouteChange:!0}};let h=(()=>{class t{constructor(t,e){this.dialogRef=t,this.data=e}ngOnInit(){}onNoClick(){this.dialogRef.close()}drop(t){Object(c.d)(this.data,t.previousIndex,t.currentIndex)}confirm(){this.dialogRef.close(this.list.options.toArray().map(t=>t.value))}selectOption(t){t.option.value.isShow=!t.option.value.isShow}}return t.\u0275fac=function(e){return new(e||t)(r.cc(i.g),r.cc(i.a))},t.\u0275cmp=r.Wb({type:t,selectors:[["notadd-field-setting-dialog"]],viewQuery:function(t,e){var n;1&t&&r.Sc(o.f,!0),2&t&&r.Hc(n=r.rc())&&(e.list=n.first)},decls:21,vars:3,consts:[["fxLayout","column","fxLayoutAlign","space-between none","fxLayoutGap","24px",1,"wrap"],["mat-dialog-title","",1,"dialog-title"],["mat-dialog-content","","fxLayout","column","fxLayoutAlign","start none",1,"dialog-content"],["fxLayoutAlign","space-between center",1,"title"],["notaddPerfectScrollbar","",1,"scroll-list",3,"notaddPerfectScrollbarOptions"],["cdkDropList","",1,"list",3,"selectionChange","cdkDropListDropped"],["list",""],["class","options","color","primary","cdkDragBoundary",".list","cdkDrag","",3,"selected","value",4,"ngFor","ngForOf"],["mat-dialog-actions","","fxLayoutAlign","end center",1,"dialog-actions"],["mat-stroked-button","",1,"action-btn",3,"click"],["mat-raised-button","","color","primary",1,"action-btn",3,"click"],["color","primary","cdkDragBoundary",".list","cdkDrag","",1,"options",3,"selected","value"],["fxLayoutAlign","space-between center","fxFlex","80"],["fxFlex","30"],["fxFlex","50"]],template:function(t,e){1&t&&(r.ic(0,"div",0),r.ic(1,"div",1),r.ic(2,"span"),r.Xc(3,"\u5b57\u6bb5\u9009\u62e9\u8bbe\u7f6e"),r.hc(),r.hc(),r.ic(4,"div",2),r.ic(5,"div",3),r.ic(6,"span"),r.Xc(7,"\u663e\u793a\u987a\u5e8f"),r.hc(),r.ic(8,"span"),r.Xc(9,"\u5b57\u6bb5\u540d"),r.hc(),r.ic(10,"span"),r.Xc(11,"\u662f\u5426\u663e\u793a"),r.hc(),r.hc(),r.ic(12,"div",4),r.ic(13,"mat-selection-list",5,6),r.qc("selectionChange",(function(t){return e.selectOption(t)}))("cdkDropListDropped",(function(t){return e.drop(t)})),r.Vc(15,p,7,4,"mat-list-option",7),r.hc(),r.hc(),r.hc(),r.ic(16,"div",8),r.ic(17,"button",9),r.qc("click",(function(){return e.onNoClick()})),r.Xc(18,"\u53d6\u6d88"),r.hc(),r.ic(19,"button",10),r.qc("click",(function(){return e.confirm()})),r.Xc(20,"\u786e\u5b9a"),r.hc(),r.hc(),r.hc()),2&t&&(r.Pb(12),r.Ac("notaddPerfectScrollbarOptions",r.Cc(2,g)),r.Pb(3),r.Ac("ngForOf",e.data))},directives:[a.d,a.c,a.e,i.h,i.e,s.a,o.f,c.b,l.s,i.c,u.b,o.e,c.a,a.b,d.a],styles:[".wrap[_ngcontent-%COMP%]{height:100%}.wrap[_ngcontent-%COMP%]   .dialog-content[_ngcontent-%COMP%]{-webkit-box-flex:1;flex-grow:1}.wrap[_ngcontent-%COMP%]   .dialog-content[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{padding:0 15px;line-height:40px;background-color:rgba(86,163,247,.2);border-radius:4px;font-size:14px;font-weight:700;color:rgba(0,0,0,.6)}.wrap[_ngcontent-%COMP%]   .dialog-content[_ngcontent-%COMP%]   .scroll-list[_ngcontent-%COMP%]{-webkit-box-flex:1;flex-grow:1}.wrap[_ngcontent-%COMP%]   .dialog-actions[_ngcontent-%COMP%]{margin-bottom:0}.wrap[_ngcontent-%COMP%]   .dialog-actions[_ngcontent-%COMP%]   .action-btn[_ngcontent-%COMP%]{width:90px;height:40px}"]}),t})()},IM4x:function(t,e,n){"use strict";n.d(e,"a",(function(){return v}));var i=n("C7z2"),o=n("Iq5M"),c=n("uzce"),r=n("HKMU"),a=n("ZC9W"),s=n("0Gyu"),l=n("Jldu");const u=["pageInput"],d=function(t){return{selected:t}};function p(t,e){if(1&t){const t=o.jc();o.ic(0,"li",21),o.qc("click",(function(){o.Mc(t);const n=e.$implicit;return o.uc(3).onSizeChange(n)})),o.Xc(1),o.hc()}if(2&t){const t=e.$implicit,n=o.uc(3);o.Ac("ngClass",o.Dc(2,d,n.pageSize===t)),o.Pb(1),o.Yc(t)}}function g(t,e){if(1&t&&(o.ic(0,"span",19),o.ic(1,"ul"),o.Vc(2,p,2,4,"li",20),o.hc(),o.hc()),2&t){const t=o.uc(2);o.Pb(2),o.Ac("ngForOf",t.pageSizeOptions)}}function h(t,e){if(1&t){const t=o.jc();o.ic(0,"div",13),o.ic(1,"div",14),o.ic(2,"span"),o.Xc(3,"\u5f53\u524d\u9875\u663e\u793a"),o.hc(),o.ic(4,"span",15),o.ic(5,"button",16),o.qc("click",(function(){return o.Mc(t),o.uc().openPageSizeOptions()})),o.ic(6,"span",17),o.Xc(7),o.hc(),o.ic(8,"mat-icon"),o.Xc(9,"keyboard_arrow_down"),o.hc(),o.hc(),o.Vc(10,g,3,1,"span",18),o.hc(),o.ic(11,"span"),o.Xc(12,"\u6761"),o.hc(),o.hc(),o.hc()}if(2&t){const t=o.uc();o.Pb(7),o.Yc(t.pageSize),o.Pb(3),o.Ac("ngIf",t.optionsOpened)}}function m(t,e){if(1&t){const t=o.jc();o.ic(0,"button",22),o.qc("click",(function(){return o.Mc(t),o.uc().loadPagesFromTheLeft()})),o.Xc(1,"..."),o.hc()}}function f(t,e){if(1&t){const t=o.jc();o.ic(0,"button",23),o.qc("click",(function(){o.Mc(t);const n=e.$implicit;return o.uc().onPageChange(n)})),o.Xc(1),o.hc()}if(2&t){const t=e.$implicit,n=o.uc();o.Ac("title",t)("ngClass",o.Dc(3,d,n.currentPage==t)),o.Pb(1),o.Yc(t)}}function b(t,e){if(1&t){const t=o.jc();o.ic(0,"button",22),o.qc("click",(function(){return o.Mc(t),o.uc().loadPagesFromTheRight()})),o.Xc(1,"..."),o.hc()}}const y=function(t,e){return{hide:t,disabled:e}},P=function(t,e){return{"circle-button":t,"tooltip-enabled":e}};let v=(()=>{class t extends i.a{constructor(t,e,n){super(t,e),this.paginatorPageChangeService=n,this.optionsOpened=!1}ngOnInit(){this.initialize()}ngOnChanges(t){t.length&&(this.initialize(),this.totalCount=t.length.currentValue)}initialize(){this.pages=[],this.visiblePages=[],this.currentPage=Math.ceil(this.currentPage||1);const t=Math.ceil(this.length/this.pageSize);this.maxVisiblePages=Math.abs(this.maxVisiblePages||5);for(let n=1;n<=t;n++)this.pages.push(n);const e=Math.max(this.currentPage-this.maxVisiblePages,0);this.visiblePages=this.pages.slice(e,e+this.maxVisiblePages),this.pagesOnTheLeft=this.pages.length&&!this.visiblePages.includes(1),this.pagesOnTheRight=this.pages.length&&!this.visiblePages.includes(this.pages.length),this.pageSizeOptionsLabel="\u6bcf\u9875\u6761\u6570",this.previousPageLabel="\u4e0a\u4e00\u9875",this.nextPageLabel="\u4e0b\u4e00\u9875",this.firstPageLabel="\u9996\u9875",this.lastPageLabel="\u5c3e\u9875",this.disableTooltip=!1}firstPage(){this.visiblePages.includes(1)||this.loadVisiblePages(0,this.maxVisiblePages),this.onPageChange(1)}lastPage(){this.visiblePages.includes(this.pages.length)||this.loadVisiblePages(-this.maxVisiblePages),this.onPageChange(this.pages.length)}previousPage(){if(this.currentPage>1){const t=this.currentPage-1;if(!this.visiblePages.includes(t)){const e=t-1;this.loadVisiblePages(e,e+this.maxVisiblePages)}this.onPageChange(t)}}nextPage(){if(this.currentPage<this.pages.length){const t=this.currentPage+1;if(!this.visiblePages.includes(t)){const e=Math.max(t-this.maxVisiblePages,0);this.loadVisiblePages(e,t)}this.onPageChange(this.currentPage+1)}}onPageChange(t){this.optionsOpened=!1;const e={pageIndex:t,previousPageIndex:this.currentPage,length:this.length,pageSize:this.pageSize};this.currentPage=t,this.page.emit(e)}onSizeChange(t){if(this.optionsOpened=!1,this.pageSize!==t){const e=Math.ceil(this.length/t);e<this.currentPage&&(this.currentPage=e),this.pageSize=t,this.initialize(),this.onPageChange(this.currentPage),this.paginatorPageChangeService.pageChange()}}openPageSizeOptions(){this.optionsOpened=!0}onPageGo(t,e){if("Enter"===t.key){let t=Number(e);t>this.pages.length?t=this.pages.length:t<1&&(t=1),this.pageInput.nativeElement.value=t,this.onPageChange(t)}}loadPagesFromTheLeft(){const t=this.visiblePages[0],e=Math.max(t-this.maxVisiblePages,1)-1;this.loadVisiblePages(e,e+this.maxVisiblePages),this.onPageChange(t-1)}loadPagesFromTheRight(){const t=this.visiblePages[this.visiblePages.length-1],e=this.pages.length;let n=t;n+this.maxVisiblePages>e&&(n=Math.max(e-this.maxVisiblePages,0)),this.loadVisiblePages(n,n+this.maxVisiblePages),this.onPageChange(t+1)}previousButtonsDisabled(){return 1===this.currentPage}nextButtonsDisabled(){return!this.pages.length||this.currentPage===this.pages.length}loadVisiblePages(t,e){this.visiblePages=this.pages.slice(t,e);const n=this.pages.length;this.pagesOnTheLeft=this.pages.length&&!this.visiblePages.includes(1),this.pagesOnTheRight=this.pages.length&&!this.visiblePages.includes(n)}}return t.\u0275fac=function(e){return new(e||t)(o.cc(i.b),o.cc(o.i),o.cc(c.a))},t.\u0275cmp=o.Wb({type:t,selectors:[["notadd-paginator"]],viewQuery:function(t,e){var n;1&t&&o.Sc(u,!0),2&t&&o.Hc(n=o.rc())&&(e.pageInput=n.first)},inputs:{currentPage:"currentPage",maxVisiblePages:"maxVisiblePages",autoHide:"autoHide",disabled:"disabled",pageSizeOptionsLabel:"pageSizeOptionsLabel",previousPageLabel:"previousPageLabel",nextPageLabel:"nextPageLabel",firstPageLabel:"firstPageLabel",lastPageLabel:"lastPageLabel",circleButton:"circleButton",disableTooltip:"disableTooltip"},features:[o.Ob([i.b]),o.Mb,o.Nb()],decls:32,vars:31,consts:[[1,"paginator-container",3,"ngClass"],["class","page-size-control",4,"ngIf"],[1,"page-change-control",3,"ngClass"],["mat-stroked-button","",1,"page-button","first-page-button",3,"disabled","matTooltip","matTooltipDisabled","matTooltipPosition","click"],["mat-stroked-button","",1,"page-button","pre-page-button",3,"disabled","matTooltip","matTooltipDisabled","matTooltipPosition","click"],["mat-stroked-button","","class","page-button",3,"click",4,"ngIf"],["mat-stroked-button","","class","page-button",3,"title","ngClass","click",4,"ngFor","ngForOf"],["mat-stroked-button","",1,"page-button","next-page-button",3,"disabled","matTooltip","matTooltipDisabled","matTooltipPosition","click"],["mat-stroked-button","",1,"page-button","last-page-button",3,"disabled","matTooltip","matTooltipDisabled","matTooltipPosition","click"],[1,"total-count"],[1,"go-page"],["type","number",1,"go-page-input",3,"max","min","keyup"],["pageInput",""],[1,"page-size-control"],["fxLayout","row","fxLayoutAlign","start center",1,"select-page"],[1,"select-page-size"],["mat-stroked-button","",3,"click"],[1,"ml-10"],["class","select-page-size-options",4,"ngIf"],[1,"select-page-size-options"],[3,"ngClass","click",4,"ngFor","ngForOf"],[3,"ngClass","click"],["mat-stroked-button","",1,"page-button",3,"click"],["mat-stroked-button","",1,"page-button",3,"title","ngClass","click"]],template:function(t,e){if(1&t){const t=o.jc();o.ic(0,"div",0),o.Vc(1,h,13,2,"div",1),o.ic(2,"div",2),o.ic(3,"button",3),o.qc("click",(function(){return e.firstPage()})),o.ic(4,"mat-icon"),o.Xc(5,"first_page"),o.hc(),o.hc(),o.ic(6,"button",4),o.qc("click",(function(){return e.previousPage()})),o.ic(7,"mat-icon"),o.Xc(8,"chevron_left"),o.hc(),o.hc(),o.Vc(9,m,2,0,"button",5),o.Vc(10,f,2,5,"button",6),o.Vc(11,b,2,0,"button",5),o.ic(12,"button",7),o.qc("click",(function(){return e.nextPage()})),o.ic(13,"mat-icon"),o.Xc(14,"chevron_right"),o.hc(),o.hc(),o.ic(15,"button",8),o.qc("click",(function(){return e.lastPage()})),o.ic(16,"mat-icon"),o.Xc(17,"last_page"),o.hc(),o.hc(),o.hc(),o.ic(18,"div",9),o.ic(19,"span"),o.Xc(20,"\u5171"),o.hc(),o.ic(21,"span"),o.Xc(22),o.hc(),o.ic(23,"span"),o.Xc(24,"\u6761"),o.hc(),o.hc(),o.ic(25,"div",10),o.ic(26,"span"),o.Xc(27,"\u8df3\u8f6c\u81f3"),o.hc(),o.ic(28,"input",11,12),o.qc("keyup",(function(n){o.Mc(t);const i=o.Ic(29);return e.onPageGo(n,i.value)})),o.hc(),o.ic(30,"span"),o.Xc(31,"\u9875"),o.hc(),o.hc(),o.hc()}2&t&&(o.Ac("ngClass",o.Ec(25,y,e.autoHide&&e.pages.length<2,e.disabled)),o.Pb(1),o.Ac("ngIf",e.pageSizeOptions&&e.pageSizeOptions.length),o.Pb(1),o.Ac("ngClass",o.Ec(28,P,e.circleButton,!e.disableTooltip)),o.Pb(1),o.Ac("disabled",e.previousButtonsDisabled())("matTooltip",e.firstPageLabel)("matTooltipDisabled",e.previousButtonsDisabled())("matTooltipPosition","above"),o.Pb(3),o.Ac("disabled",e.previousButtonsDisabled())("matTooltip",e.previousPageLabel)("matTooltipDisabled",e.previousButtonsDisabled())("matTooltipPosition","above"),o.Pb(3),o.Ac("ngIf",e.pagesOnTheLeft),o.Pb(1),o.Ac("ngForOf",e.visiblePages),o.Pb(1),o.Ac("ngIf",e.pagesOnTheRight),o.Pb(1),o.Ac("disabled",e.nextButtonsDisabled())("matTooltip",e.nextPageLabel)("matTooltipDisabled",e.nextButtonsDisabled())("matTooltipPosition","above"),o.Pb(3),o.Ac("disabled",e.nextButtonsDisabled())("matTooltip",e.lastPageLabel)("matTooltipDisabled",e.nextButtonsDisabled())("matTooltipPosition","above"),o.Pb(7),o.Yc(e.totalCount),o.Pb(6),o.Ac("max",e.pages.length)("min",1))},directives:[r.q,r.t,a.b,s.b,l.a,r.s],styles:[".paginator-container[_ngcontent-%COMP%]{display:-webkit-box;display:flex;color:rgba(0,0,0,.54);font-size:14px;-webkit-box-align:center;align-items:center;-webkit-box-pack:end;justify-content:flex-end;padding:24px}.paginator-container.disabled[_ngcontent-%COMP%]{pointer-events:none;opacity:.5}.paginator-container.hide[_ngcontent-%COMP%]{display:none}.paginator-container[_ngcontent-%COMP%]   .page-size-control[_ngcontent-%COMP%]{display:-webkit-box;display:flex}.paginator-container[_ngcontent-%COMP%]   .page-size-control-label[_ngcontent-%COMP%]{margin:auto 10px auto 0}.paginator-container[_ngcontent-%COMP%]   .page-size-control[_ngcontent-%COMP%]   .select-page[_ngcontent-%COMP%]{margin-right:16px;position:relative;height:30px;line-height:30px}.paginator-container[_ngcontent-%COMP%]   .page-size-control[_ngcontent-%COMP%]   .select-page-size[_ngcontent-%COMP%]{text-align:center;min-width:40px;position:relative;padding-right:8px;padding-left:8px}.paginator-container[_ngcontent-%COMP%]   .page-size-control[_ngcontent-%COMP%]   .select-page-size[_ngcontent-%COMP%]   .ml-10[_ngcontent-%COMP%]{margin-left:10px;vertical-align:0}.paginator-container[_ngcontent-%COMP%]   .page-size-control[_ngcontent-%COMP%]   .select-page-size[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{text-align:center;height:30px;min-width:40px;line-height:30px;color:rgba(0,0,0,.38);padding:0}.paginator-container[_ngcontent-%COMP%]   .page-size-control[_ngcontent-%COMP%]   .select-page-size-options[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]{position:absolute;left:0;top:-100%;z-index:999;max-height:136px;overflow-y:auto;width:100%;background-color:#fff;padding:0;margin-bottom:0;margin-top:0;border-radius:4px;box-shadow:0 3px 5px -1px rgba(0,0,0,.2),0 6px 10px 0 rgba(0,0,0,.14),0 1px 18px 0 rgba(0,0,0,.12);-webkit-transition:all 375ms ease-in-out;transition:all 375ms ease-in-out}.paginator-container[_ngcontent-%COMP%]   .page-size-control[_ngcontent-%COMP%]   .select-page-size-options[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{position:relative;float:left;width:100%;background-color:#fff;list-style-type:none;padding:8px 0;margin:0;-webkit-transition:all 275ms ease-in-out;transition:all 275ms ease-in-out;display:block;cursor:pointer;text-align:center}.paginator-container[_ngcontent-%COMP%]   .page-size-control[_ngcontent-%COMP%]   .select-page-size-options[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]:first-child{border-radius:4px 4px 0 0}.paginator-container[_ngcontent-%COMP%]   .page-size-control[_ngcontent-%COMP%]   .select-page-size-options[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]:last-child{border-radius:0 0 4px 4px}.paginator-container[_ngcontent-%COMP%]   .page-size-control[_ngcontent-%COMP%]   .select-page-size-options[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li.selected[_ngcontent-%COMP%], .paginator-container[_ngcontent-%COMP%]   .page-size-control[_ngcontent-%COMP%]   .select-page-size-options[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]:hover{background-color:#eee}.page-button[_ngcontent-%COMP%]{color:rgba(0,0,0,.38)}mat-icon[_ngcontent-%COMP%]{margin-top:-4px}.page-change-control[_ngcontent-%COMP%]{display:-webkit-box;display:flex}.page-change-control[_ngcontent-%COMP%]   .page-button[_ngcontent-%COMP%]{position:relative;padding:0;min-width:30px;height:30px;line-height:30px;flex-shrink:0;font-size:14px;margin-right:8px}.page-change-control[_ngcontent-%COMP%]   .page-button.selected[_ngcontent-%COMP%]:not([disabled]), .page-change-control[_ngcontent-%COMP%]   .page-button[_ngcontent-%COMP%]:hover:not([disabled]){background-color:#448aff;color:#fff;cursor:pointer;border:none;box-shadow:0 3px 1px -2px rgba(0,0,0,.2),0 2px 2px 0 rgba(0,0,0,.14),0 1px 5px 0 rgba(0,0,0,.12)}.page-change-control[_ngcontent-%COMP%]   .first-page-button[_ngcontent-%COMP%], .page-change-control[_ngcontent-%COMP%]   .last-page-button[_ngcontent-%COMP%], .page-change-control[_ngcontent-%COMP%]   .next-page-button[_ngcontent-%COMP%], .page-change-control[_ngcontent-%COMP%]   .pre-page-button[_ngcontent-%COMP%]{font-weight:400;font-size:12px}.page-change-control.circle-button[_ngcontent-%COMP%]   .page-button[_ngcontent-%COMP%]{border-radius:50%}.page-change-control[_ngcontent-%COMP%]   button.page-button[_ngcontent-%COMP%]:last-child{margin-right:0}.total-count[_ngcontent-%COMP%]{height:30px;line-height:30px}.go-page[_ngcontent-%COMP%], .total-count[_ngcontent-%COMP%]{margin-left:16px}.go-page-input[_ngcontent-%COMP%]{margin:0 8px;width:40px;height:30px;border-radius:4px;border:1px solid #ccc;text-align:center}.go-page[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]::-webkit-inner-spin-button, .go-page[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]::-webkit-outer-spin-button{-webkit-appearance:none!important}.go-page[_ngcontent-%COMP%]   input[type=number][_ngcontent-%COMP%]{-moz-appearance:textfield}"]}),t})()},YkUH:function(t,e,n){"use strict";n.d(e,"a",(function(){return f}));var i=n("Iq5M"),o=n("LgQE"),c=n("1yWB"),r=n("8mwJ"),a=n("tet4"),s=n("/UCm"),l=n("HKMU"),u=n("ZC9W"),d=n("0Gyu"),p=n("Jldu");function g(t,e){if(1&t){const t=i.jc();i.ic(0,"button",8),i.qc("click",(function(){return i.Mc(t),i.uc().onAddClick()})),i.ic(1,"mat-icon",9),i.Xc(2,"add"),i.hc(),i.Xc(3," \u65b0\u589e "),i.hc()}if(2&t){const t=i.uc();i.Ac("disabled",t.disableAdd)}}function h(t,e){1&t&&i.ec(0)}function m(t,e){if(1&t){const t=i.jc();i.ic(0,"button",10),i.qc("click",(function(){return i.Mc(t),i.uc().onSortClick()})),i.ic(1,"mat-icon",11),i.Xc(2,"widgets"),i.hc(),i.hc()}}let f=(()=>{class t{constructor(t,e){this.dialog=t,this.ngForage=e,this.isRefreshing=!1,this.add=new i.r,this.refresh=new i.r,this.sort=new i.r}ngOnInit(){this.localForageKey&&this.ngForage.getItem(this.localForageKey).then(t=>{t&&(this.columns=t)}),this.configOptions?Object.keys(this.configOptions).includes("isShowSortButton")||(this.configOptions.isShowSortButton=!0):this.configOptions={isShowAddButton:!0,isShowSortButton:!0}}onAddClick(){this.add.emit(!0)}onRefreshClick(){this.refresh.emit(!0)}onSortClick(){this.dialog.open(c.a,{width:"480px",height:"533px",data:this.columns}).afterClosed().pipe(Object(o.a)(1)).subscribe(t=>{t&&(this.columns=t,this.sort.emit(this.columns),this.ngForage.setItem(this.localForageKey,this.columns))})}}return t.\u0275fac=function(e){return new(e||t)(i.cc(r.b),i.cc(a.d))},t.\u0275cmp=i.Wb({type:t,selectors:[["notadd-table-tool"]],contentQueries:function(t,e,n){var o;1&t&&i.Rc(n,i.W,!0),2&t&&i.Hc(o=i.rc())&&(e.template=o.first)},inputs:{disableAdd:"disableAdd",localForageKey:"localForageKey",isRefreshing:"isRefreshing",configOptions:"configOptions",columns:"columns"},outputs:{add:"add",refresh:"refresh",sort:"sort"},decls:10,vars:5,consts:[["fxLayoutAlign","space-between center",1,"actions"],["fxLayout","row","fxLayoutAlign","start center","fxLayoutGap","8px",1,"left",2,"padding","8px 0"],["class","add","mat-raised-button","","color","primary","fxLayoutAlign","center center",3,"disabled","click",4,"ngIf"],[4,"ngTemplateOutlet"],["fxLayoutAlign","space-between",1,"right-btns"],["mat-icon-button","","matTooltip","\u5237\u65b0",1,"refresh-btn","s-20",3,"click"],[1,"refresh-icon","s-20"],["class","sort-btn","mat-icon-button","","matTooltip","\u5b57\u6bb5\u9009\u62e9\u548c\u6392\u5e8f",3,"click",4,"ngIf"],["mat-raised-button","","color","primary","fxLayoutAlign","center center",1,"add",3,"disabled","click"],[1,"s-18"],["mat-icon-button","","matTooltip","\u5b57\u6bb5\u9009\u62e9\u548c\u6392\u5e8f",1,"sort-btn",3,"click"],[1,"sort-icon","s-20"]],template:function(t,e){1&t&&(i.ic(0,"div",0),i.ic(1,"div",1),i.Vc(2,g,4,1,"button",2),i.Vc(3,h,1,0,"ng-container",3),i.hc(),i.dc(4,"span"),i.ic(5,"div",4),i.ic(6,"button",5),i.qc("click",(function(){return e.onRefreshClick()})),i.ic(7,"mat-icon",6),i.Xc(8,"autorenew"),i.hc(),i.hc(),i.Vc(9,m,3,0,"button",7),i.hc(),i.hc()),2&t&&(i.Pb(2),i.Ac("ngIf",e.configOptions.isShowAddButton),i.Pb(1),i.Ac("ngTemplateOutlet",e.template),i.Pb(4),i.Uc("animation-name",e.isRefreshing?"refresh":""),i.Pb(2),i.Ac("ngIf",e.configOptions.isShowSortButton))},directives:[s.c,s.d,s.e,l.t,l.A,u.b,d.b,p.a],styles:[".actions[_ngcontent-%COMP%]{padding:8px 16px 8px 24px}.actions[_ngcontent-%COMP%]   .add[_ngcontent-%COMP%]{line-height:37px}.actions[_ngcontent-%COMP%]   .right-btns[_ngcontent-%COMP%]   .refresh-btn[_ngcontent-%COMP%]{background-color:#fff}.actions[_ngcontent-%COMP%]   .right-btns[_ngcontent-%COMP%]   .refresh-btn[_ngcontent-%COMP%]   .refresh-icon[_ngcontent-%COMP%]{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-timing-function:linear;animation-timing-function:linear;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;color:#000;opacity:.37}.actions[_ngcontent-%COMP%]   .right-btns[_ngcontent-%COMP%]   .sort-btn[_ngcontent-%COMP%]   .sort-icon[_ngcontent-%COMP%]{color:#000;opacity:.37}@-webkit-keyframes refresh{0%{-webkit-transform:rotate(0);transform:rotate(0)}to{-webkit-transform:rotate(180deg);transform:rotate(180deg)}}@keyframes refresh{0%{-webkit-transform:rotate(0);transform:rotate(0)}to{-webkit-transform:rotate(180deg);transform:rotate(180deg)}}"]}),t})()},qkUZ:function(t,e,n){"use strict";n.d(e,"p",(function(){return s})),n.d(e,"n",(function(){return d})),n.d(e,"i",(function(){return h})),n.d(e,"k",(function(){return b})),n.d(e,"s",(function(){return v})),n.d(e,"E",(function(){return O})),n.d(e,"t",(function(){return w})),n.d(e,"g",(function(){return _})),n.d(e,"c",(function(){return D})),n.d(e,"u",(function(){return z})),n.d(e,"D",(function(){return B})),n.d(e,"o",(function(){return W})),n.d(e,"e",(function(){return N})),n.d(e,"b",(function(){return H})),n.d(e,"q",(function(){return G})),n.d(e,"y",(function(){return Z})),n.d(e,"a",(function(){return nt})),n.d(e,"j",(function(){return ct})),n.d(e,"m",(function(){return st})),n.d(e,"r",(function(){return dt})),n.d(e,"F",(function(){return ht})),n.d(e,"z",(function(){return bt})),n.d(e,"d",(function(){return vt})),n.d(e,"f",(function(){return Ot})),n.d(e,"h",(function(){return wt})),n.d(e,"v",(function(){return _t})),n.d(e,"B",(function(){return Dt})),n.d(e,"x",(function(){return zt})),n.d(e,"l",(function(){return Bt})),n.d(e,"w",(function(){return Wt})),n.d(e,"A",(function(){return Nt})),n.d(e,"C",(function(){return Ht}));var i=n("ktN7"),o=n.n(i),c=n("Umbz"),r=n("Iq5M");const a=o.a`
    query dutyServiceStationDeployWarningList($where: DutyServiceStationDeployWarningListInputWhere, $limit: Limit, $order: DutyServiceStationDeployWarningListInputOrder) {
  dutyServiceStationDeployWarningList(where: $where, limit: $limit, order: $order) {
    count
    list {
      station {
        id
        code
        title
      }
      department {
        id
        title
      }
      dailyDate
      classes {
        time {
          startTime {
            hour
            minute
          }
          endTime {
            hour
            minute
          }
        }
        planPoliceCount
        planTotalCount
        policeCount
        totalCount
      }
    }
  }
}
    `;let s=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=a,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return l(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const l=r.kc(s),u=o.a`
    query dutyServiceStationDepartmentList($where: DutyServiceStationDepartmentListInputWhere, $limit: Limit, $order: DutyServiceStationDepartmentListInputOrder) {
  dutyServiceStationDepartmentList(where: $where, limit: $limit, order: $order) {
    count
    list {
      title
      id
      totalPolice
      stationCount
      policeCount
    }
  }
}
    `;let d=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=u,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return p(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const p=r.kc(d),g=o.a`
    query dutyServicePoliceAdjustList($where: DutyServicePoliceAdjustListInputWhere, $limit: Limit, $order: DutyServicePoliceAdjustListInputOrder) {
  dutyServicePoliceAdjustList(where: $where, limit: $limit, order: $order) {
    count
    list {
      id
      type
      reportPersonnel {
        id
        realName
        department {
          title
        }
      }
      reportPersonnelId
      substitutePersonnel {
        id
        realName
      }
      station {
        id
        title
      }
      substitutePersonnelId
      times {
        time {
          weeks
          startTime {
            hour
            minute
          }
          endTime {
            hour
            minute
          }
        }
      }
      startDate
      endDate
      createDate
      desc
    }
  }
}
    `;let h=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=g,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return m(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const m=r.kc(h),f=o.a`
    query dutyServiceStationAllAdjustmentRecord($where: DutyServiceStationAllInputWhere) {
  dutyServiceStationAll(where: $where) {
    id
    title
    departmentId
    startDate
    endDate
  }
}
    `;let b=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=f,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return y(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const y=r.kc(b),P=o.a`
    query dutyServiceStationTimeAll($where: DutyServiceStationTimeAllInput) {
  dutyServiceStationTimeAll(where: $where) {
    id
    stationId
    weeks
    startTime {
      hour
      minute
    }
    endTime {
      hour
      minute
    }
    createDate
  }
}
    `;let v=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=P,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return S(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const S=r.kc(v),x=o.a`
    query personnelInformationAll($where: PersonnelInformationAllInput) {
  personnelInformationAll(where: $where) {
    id
    realName
  }
}
    `;let O=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=x,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return C(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const C=r.kc(O),k=o.a`
    query dutyServiceStationTimePersonnelAll($where: DutyServiceStationTimePersonnelAllInputWhere) {
  dutyServiceStationTimePersonnelAll(where: $where) {
    personnelDetail {
      id
      realName
    }
  }
}
    `;let w=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=k,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return M(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const M=r.kc(w),I=o.a`
    mutation dutyServicePoliceAdjustAdd($entity: DutyServicePoliceAdjustAddInput!, $timeIds: [Int]!) {
  dutyServicePoliceAdjustAdd(entity: $entity, timeIds: $timeIds)
}
    `;let _=(()=>{class t extends c.c{constructor(){super(...arguments),this.document=I,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return L(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const L=r.kc(_),T=o.a`
    query departmentTeamListOfCurrentPolice {
  departmentTeamList {
    list {
      id
      title
      children {
        title
        id
      }
    }
  }
}
    `;let D=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=T,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return $(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const $=r.kc(D),A=o.a`
    query dutyServiceStationTimePersonnelListOfCurrent($where: DutyServiceStationTimeLogListInputWhere, $limit: Limit, $order: DutyServiceStationTimeLogListInputOrder) {
  dutyServiceStationTimeLogTodayList(where: $where, limit: $limit, order: $order) {
    count
    list {
      id
      time {
        id
        weeks
        startTime {
          hour
          minute
        }
        endTime {
          hour
          minute
        }
      }
      station {
        id
        title
        startDate
        endDate
      }
      timeId
      personnelId
      department {
        title
      }
      personnel {
        id
        policeCode
        personCode
        realName
      }
    }
  }
}
    `;let z=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=A,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return q(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const q=r.kc(z),Y=o.a`
    query dutyServiceWorkloadDepartmentList($where: DutyServiceWorkloadDepartmentListInputWhere, $limit: Limit, $order: DutyServiceWorkloadDepartmentListInputOrder) {
  dutyServiceWorkloadDepartmentList(where: $where, limit: $limit, order: $order) {
    count
    list {
      id
      title
      department {
        id
        title
        planPoliceTime
        planTotalTime
      }
    }
  }
}
    `;let B=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=Y,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return V(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const V=r.kc(B),X=o.a`
    query dutyServiceStationDeployPlanList($where: DutyServiceStationDeployPlanListInputWhere, $limit: Limit, $order: DutyServiceStationDeployPlanListInputOrder) {
  dutyServiceStationDeployPlanList(where: $where, limit: $limit, order: $order) {
    count
    list {
      code
      title
      departmentId
      parentDepartment {
        title
        id
      }
      department {
        title
        id
      }
      times {
        totalPolice
        policeCount
        id
        startTime {
          hour
          minute
        }
        endTime {
          hour
          minute
        }
      }
    }
  }
}
    `;let W=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=X,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return j(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const j=r.kc(W),F=o.a`
    query dutyServiceBasicDataList {
  dutyServiceBasicDataList {
    policeAdjustType {
      code
      title
      id
    }
    stationStatus {
      code
      title
      id
    }
    weeks {
      title
      code
      id
    }
    personnelType {
      code
      title
      id
    }
  }
}
    `;let N=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=F,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return R(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const R=r.kc(N),U=o.a`
    query departmentTeamListOfBasic {
  departmentTeamList {
    list {
      id
      title
      children {
        title
        id
      }
    }
  }
}
    `;let H=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=U,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return K(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const K=r.kc(H),E=o.a`
    query dutyServiceStationList($where: DutyServiceStationListInputWhere, $limit: Limit, $order: DutyServiceStationListInputOrder) {
  dutyServiceStationList(limit: $limit, where: $where, order: $order) {
    count
    list {
      id
      code
      title
      type {
        id
        title
        desc
        createDate
        createUserId
        departmentId
        domainId
      }
      typeId
      department {
        title
        principal
      }
      departmentId
      peoplePoliceCount
      auxiliaryPoliceCount
      startDate
      endDate
      desc
      createDate
      createUserId
      createDepartmentId
      domainId
      times {
        weeks
        startTime {
          hour
          minute
        }
        endTime {
          hour
          minute
        }
      }
    }
  }
}
    `;let G=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=E,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return J(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const J=r.kc(G),Q=o.a`
    query dutyServiceStationTypeAll {
  dutyServiceStationTypeAll {
    id
    title
    code
    desc
  }
}
    `;let Z=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=Q,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return tt(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const tt=r.kc(Z),et=o.a`
    query departmentTeamList {
  departmentTeamList {
    list {
      id
      code
      title
      level
      children {
        title
        id
        code
        level
      }
    }
  }
}
    `;let nt=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=et,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return it(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const it=r.kc(nt),ot=o.a`
    mutation dutyServiceStationAdd($entity: DutyServiceStationAddInput!, $times: [DutyServiceStationAddInputTimes]!) {
  dutyServiceStationAdd(entity: $entity, times: $times)
}
    `;let ct=(()=>{class t extends c.c{constructor(){super(...arguments),this.document=ot,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return rt(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const rt=r.kc(ct),at=o.a`
    mutation dutyServiceStationDeleteById($id: Int!) {
  dutyServiceStationDeleteById(id: $id)
}
    `;let st=(()=>{class t extends c.c{constructor(){super(...arguments),this.document=at,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return lt(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const lt=r.kc(st),ut=o.a`
    query dutyServiceStationListOfSort($where: DutyServiceStationListInputWhere, $limit: Limit, $order: DutyServiceStationListInputOrder) {
  dutyServiceStationList(where: $where, limit: $limit, order: $order) {
    count
    list {
      id
      code
      title
      desc
      peoplePoliceCount
      auxiliaryPoliceCount
      type {
        id
        code
        title
        desc
        createDate
      }
      department {
        id
        code
        title
        principal
      }
      startDate
      endDate
      times {
        id
        weeks
        startTime {
          hour
          minute
        }
        endTime {
          hour
          minute
        }
        personnel {
          id
          personnelDetail {
            id
            personCode
            policeCode
            realName
            mobileNumber
          }
        }
      }
    }
  }
}
    `;let dt=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=ut,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return pt(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const pt=r.kc(dt),gt=o.a`
    query personnelInformationList($where: PersonnelInformationListInput, $limit: ListLimit) {
  personnelInformationList(where: $where, limit: $limit) {
    count
    list {
      id
      personCode
      policeType
      idTypeCodeOfPublicSecurityOrganStaff
      realName
      mobileNumber
    }
  }
}
    `;let ht=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=gt,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return mt(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const mt=r.kc(ht),ft=o.a`
    query dutyServiceStationTypeAllOfSort {
  dutyServiceStationTypeAll {
    id
    title
  }
}
    `;let bt=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=ft,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return yt(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const yt=r.kc(bt),Pt=o.a`
    query departmentTeamListOfSort {
  departmentTeamList {
    list {
      id
      title
      children {
        title
        id
      }
    }
  }
}
    `;let vt=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=Pt,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return St(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const St=r.kc(vt),xt=o.a`
    query dutyServiceBasicDataListOfSort {
  dutyServiceBasicDataList {
    personnelType {
      code
      title
      id
    }
  }
}
    `;let Ot=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=xt,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return Ct(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const Ct=r.kc(Ot),kt=o.a`
    query dutyServicePoliceAdjustAllOfSort($where: DutyServicePoliceAdjustAllInputWhere) {
  dutyServicePoliceAdjustAll(where: $where) {
    id
    type
    startDate
    endDate
    desc
    reportPersonnel {
      id
      realName
    }
    substitutePersonnel {
      id
      realName
    }
  }
}
    `;let wt=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=kt,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return Mt(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const Mt=r.kc(wt),It=o.a`
    mutation dutyServiceStationTimePersonnelUpdates($input: [DutyServiceStationTimePersonnelUpdatesInput]!) {
  dutyServiceStationTimePersonnelUpdates(input: $input)
}
    `;let _t=(()=>{class t extends c.c{constructor(){super(...arguments),this.document=It,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return Lt(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const Lt=r.kc(_t),Tt=o.a`
    query dutyServiceStationTypeList($where: DutyServiceStationTypeListInputWhere, $limit: Limit, $order: DutyServiceStationTypeListInputOrder) {
  dutyServiceStationTypeList(where: $where, limit: $limit, order: $order) {
    list {
      id
      code
      title
      desc
      createDate
      createName
    }
    count
  }
}
    `;let Dt=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=Tt,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return $t(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const $t=r.kc(Dt),At=o.a`
    query dutyServiceStationTypeAll2 {
  dutyServiceStationTypeAll {
    id
    title
  }
}
    `;let zt=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=At,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return qt(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const qt=r.kc(zt),Yt=o.a`
    query dutyServiceStationAll {
  dutyServiceStationAll {
    type {
      title
    }
  }
}
    `;let Bt=(()=>{class t extends c.d{constructor(){super(...arguments),this.document=Yt,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return Vt(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const Vt=r.kc(Bt),Xt=o.a`
    mutation dutyServiceStationTypeAdd($entity: DutyServiceStationTypeAddInput!) {
  dutyServiceStationTypeAdd(entity: $entity)
}
    `;let Wt=(()=>{class t extends c.c{constructor(){super(...arguments),this.document=Xt,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return jt(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const jt=r.kc(Wt),Ft=o.a`
    mutation dutyServiceStationTypeDeleteById($id: Int!) {
  dutyServiceStationTypeDeleteById(id: $id)
}
    `;let Nt=(()=>{class t extends c.c{constructor(){super(...arguments),this.document=Ft,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return Rt(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const Rt=r.kc(Nt),Ut=o.a`
    mutation dutyServiceStationTypeUpdateById($id: Int!, $entity: DutyServiceStationTypeUpdateInputEntity!) {
  dutyServiceStationTypeUpdateById(id: $id, entity: $entity)
}
    `;let Ht=(()=>{class t extends c.c{constructor(){super(...arguments),this.document=Ut,this.client="gateway-duty-service-management"}}return t.\u0275fac=function(e){return Kt(e||t)},t.\u0275prov=r.Yb({token:t,factory:t.\u0275fac,providedIn:"root"}),t})();const Kt=r.kc(Ht)}}]);