const GJPacketType = {
    NONE : 0,
    VIEDO: 1,
    AUDIO: 2,
    INFO: 3,
    OK : 4,
    ERROR : 5,
    STOP : 6
};

const GJMessageType = {
    //PalyerMgr
    CMD_CLEAR:1,
    CMD_TERMINATE:2,
    //PLAYER
    CMD_PLAY:101,//OK
    CMD_ACK:102,//OK
    CMD_STOP:103,//OK
    CMD_PAUSE:104,//OK
    CMD_RESUME:105,//OK
    CMD_VOLUME:106,//OK
    CMD_PLAYBACK:107,//OK
    CMD_SNAPSHOT:108,//OK
    CMD_MUTE:109,//OK
    CMD_UNPLAYBACK:110,//OK
    //RECEVICER
    CMD_RECEVICER_START:201,
    CMD_RECEVICER_STOP:202,
    //DECODER
    CMD_DECODER_OPEN:301,
    CMD_DECODER_CLOST:302,
    //EVENT
    ON_INFO:501,
    ON_PACKET:502,
    ON_VIDEO_FRAME:503,
    ON_AUDIO_FRAME:504,
    ON_SNAPSHOT:505,
    ON_RESIZE:506,
    //NONE
    NONE:0
}