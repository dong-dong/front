(function (window) {

    /* Change this variable to update the environment variable value */
    const environmentVariable = {
        client: 'gateway-video-integrated-control-center',
        apiUrl: 'http://10.0.20.51:3118/graphql',
        wsUrl: 'ws://10.0.20.51:3118/graphql',
        domainId: 1,
        amapUrl: '10.0.20.51'
    };

    window.__env = window.__env || {};

    window.__env = environmentVariable;

}(this));
