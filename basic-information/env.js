(function (window) {

    /* Change this variable to update the environment variable value */
    const environmentVariable = {
        client: 'gateway-basic-information',
        apiUrl: 'http://10.0.20.51:3116/graphql',
        wsUrl: 'ws://10.0.20.51:3116/graphql',
        uploadUrl: 'http://10.0.1.212:8888',
        domainId: 1
    };

    window.__env = window.__env || {};

    window.__env = environmentVariable;

}(this));
