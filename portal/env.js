(function (window) {

    /* Change this variable to update the environment variable value */
    const environmentVariable = {
        client: 'gateway-portal',
        apiUrl: 'http://10.0.20.51:3121/graphql',
        wsUrl: 'ws://10.0.20.51:3121/graphql',
        domainId: 1
    };

    window.__env = window.__env || {};

    window.__env = environmentVariable;

}(this));
