(function (window) {

    /* Change this variable to update the environment variable value */
    const environmentVariable = {
        client: 'gateway-front',
        apiUrl: 'http://10.0.20.51:3129/graphql',
        // apiUrl: 'http://10.0.0.38:3005/graphql',
        wsUrl: 'ws://10.0.20.51:3129/graphql',
        uploadUrl: 'http://10.0.1.212:8888',
        domainId: 1,
        loginUrl: '/auth/login',
        // vid: '5ee1922ae7b3ff69253971d3', // 本地用 localhost
        vid: '5ec78a49a11e5cce5fdadfb7', // 测试用 10.0.1.25
        ngForageName: 'WEB_OPEN_PLATFORM_FRONT'
    };

    window.__env = window.__env || {};

    window.__env = environmentVariable;

}(this));
