const GJ_DECODER_TYPE_NONE = 0;
const GJ_DECODER_TYPE_LIVE = 1;
const GJ_DECODER_TYPE_ARCHIVE = 2;

self.Module = {
    onRuntimeInitialized: function () {
        onWasmLoaded();
    }
};

function strToHexCharCode(str) {
    if (str === "")
        return "";
    let hexCharCode = [];
    for (let i = 0; i < str.length; i++) {
        hexCharCode.push((str.charCodeAt(i)).toString(16));
    }
    return hexCharCode.join("");
}

function hexCharCodeToStr(hexCharCodeStr) {
    let trimedStr = hexCharCodeStr.trim();
    let rawStr = trimedStr;
    let len = rawStr.length;
    if (len % 2 !== 0) {
        return "";
    }
    let curCharCode;
    let resultStr = [];
    for (let i = 0; i < len; i = i + 2) {
        curCharCode = parseInt(rawStr.substr(i, 2), 16);
        resultStr.push(curCharCode); // ASCII Code Value
    }
    let buffer = new ArrayBuffer(len / 2);
    let uint8Buffer = new Uint8Array(buffer);
    uint8Buffer.set(resultStr, 0);
    return uint8Buffer;
}

self.importScripts("common.js");
self.importScripts("libffmpeg.js");

function Decoder() {
    this.coreLogLevel = 2;
    this.wasmLoaded = false;
    this.tmpReqQue = [];
    this.cacheBuffer = null;
    this.decodeTimer = null;
    this.videoCallback = null;
    this.audioCallback = null;
}
Decoder.prototype.setCallback = function () {

    this.videoCallback = Module.addFunction(function (streamId, buff, size, timestamp) {
        // console.log("Decoder.prototype.videoCallback");
        var outArray = Module.HEAPU8.subarray(buff, buff + size);
        var data = new Uint8Array(outArray);
        var objData = {
            type: GJMessageType.ON_VIDEO_FRAME,
            data: {
                streamId: streamId,
                timestamp: timestamp,
                data: data
            }
        };
        // console.log("decoder.postMessage.kVideoFrame");
        // console.log("decoder postMessage >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        // console.log(objData);
        // console.log("decoder postMessage <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        self.postMessage(objData, [objData.data.data.buffer]);

    });

    this.audioCallback = Module.addFunction(function (streamId, buff, size, timestamp) {
        // console.log("Decoder.prototype.audioCallback");
        var outArray = Module.HEAPU8.subarray(buff, buff + size);
        var data = new Uint8Array(outArray);
        var objData = {
            type: GJMessageType.ON_AUDIO_FRAME,
            data: {
                streamId: streamId,
                timestamp: timestamp,
                data: data
            }
        };
        // console.log("decoder.postMessage.kAudioFrame");
        // console.log("decoder postMessage >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        // console.log(objData);
        // console.log("decoder postMessage <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        self.postMessage(objData, [objData.data.data.buffer]);
    });

    this.statusCallback = Module.addFunction(function (streamId, type) {
        console.log("streamId:" + streamId + " type:" + type);
    });

    this.snapshotCallback = Module.addFunction(function (streamId, buff, size) {
        var outArray = Module.HEAPU8.subarray(buff, buff + size);
        var data = new Uint8Array(outArray);
        console.log("snapshotCallback");
        console.log(data.length);
        var objData = {
            type: GJMessageType.ON_SNAPSHOT,
            data: {
                streamId: streamId,
                data: data
            }
        };
        self.postMessage(objData, [objData.data.data.buffer]);
    });

    var ret = Module._setCallback(
        this.statusCallback,
        this.snapshotCallback,
        this.videoCallback,
        this.audioCallback
    );
}

Decoder.prototype.openDecoder = function (streamId, info) {
    //TODO 需要防止重复调用

    console.log(` Decoder.prototype.openDecoder[ ${streamId} ] ${info}`);
    let video = info.video;
    let audio = info.audio;
    let videoExtradata = Module._malloc(video.extradata_size);
    let audioExtradata = Module._malloc(audio.extradata_size);
    let veArray = new Uint8Array(hexCharCodeToStr(video.extradata));
    let aeArray = new Uint8Array(hexCharCodeToStr(audio.extradata));
    Module.HEAPU8.set(veArray, videoExtradata);
    Module.HEAPU8.set(aeArray, audioExtradata);

    // ErrorCode openDecoder(
    //     int streamId,
    //     int decoderType，
    //     int iVideoCodecID,
    //     int iVideoPixFmt,
    //     int videoWidth,
    //     int videoHeight,
    //     int iVideoTimeBaseDen,
    //     unsigned char * videoExtradata,
    //     int videoExtradataSize,
    //     int iAudioCodecID,
    //     int iAudioSampleFmt,
    //     int audioChannels,
    //     int audioSampleRate,
    //     int iAudioTimeBaseDen,
    //     uint8_t * audioExtradata,
    //     int audioExtradataSize,
    // )

    var ret = Module._openDecoder(
        streamId,
        video.codec_id,
        video.pix_fmt,
        video.width,
        video.height,
        video.rate,
        videoExtradata,
        video.extradata_size,
        audio.codec_id,
        audio.sample_fmt,
        audio.channels,
        audio.sample_rate,
        audio.rate,
        audioExtradata,
        audio.extradata_size
    );

    console.log("openDecoder return " + ret);

    Module._free(videoExtradata);
    Module._free(audioExtradata);

    var objData = {
        type: GJMessageType.CMD_DECODER_OPEN
    };
    self.postMessage(objData);


};

Decoder.prototype.closeDecoder = function () {
    console.log("closeDecoder.");
    if (this.decodeTimer) {
        clearInterval(this.decodeTimer);
        this.decodeTimer = null;
        console.log("Decode timer stopped.");
    }

    var ret = Module._closeDecoder();
    console.log("Close ffmpeg decoder return " + ret + ".");

    var objData = {
        t: kCloseDecoderRsp,
        e: 0
    };
    self.postMessage(objData);
};


Decoder.prototype.onPacket = function (streamId, type, timestamp, payload) { //向C语言发送数据
    // var typedArray = new Uint8Array(data);
    Module.HEAPU8.set(payload, this.cacheBuffer);
    // console.log("onPacket:"+streamId);
    Module._onPacket(streamId, this.cacheBuffer, payload.length, type, timestamp); //int sendData(unsigned char *buff, int size)
};

Decoder.prototype.pause = function (streamId) {
    console.log(`Decoder.prototype.pause[${streamId}]`);
    Module._pausePlay(streamId);
};

Decoder.prototype.resume = function (streamId) {
    console.log("Decoder.prototype.resume");
    Module._resumePaly(streamId);
};

Decoder.prototype.volume = function (streamId, volume) {
    console.log(`Decoder.prototype.volume(${streamId},${volume})`);
    Module._setVolume(streamId, volume);
}

Decoder.prototype.mute = function (streamId, isMute) {
    console.log(`Decoder.prototype.mute(${streamId},${isMute})`);
    if (isMute) {
        Module._setAudioStreamId(-1);
    } else {
        Module._setAudioStreamId(streamId);
    }
}

Decoder.prototype.playback = function (streamId) {
    console.log("Decoder.prototype.playback");
    Module._playback(streamId);
};

Decoder.prototype.unplayback = function (streamId) {
    console.log("Decoder.prototype.unplayback");
    Module._unplayback(streamId);
};

Decoder.prototype.snapshot = function (streamId) {
    console.log("Decoder.prototype.snapshot");
    Module._snapshot(streamId);
};

Decoder.prototype.processReq = function (req) {
    switch (req.type) {
        case GJMessageType.ON_PACKET: {
            switch (req.packetType) {
                case GJPacketType.VIEDO:
                case GJPacketType.AUDIO:
                    this.onPacket(req.streamId, req.packetType, req.timestamp, req.payload);
                    break;
                default:
                    throw new Error("Decoder error packetType");
            }
        }
        break;
    case GJMessageType.CMD_DECODER_OPEN: {
        let info = {};
        try {
            info = JSON.parse(req.message);
        } catch (error) {
            console.log(error);
            throw new Error(error);
        }
        this.openDecoder(req.streamId, info);
    }
    break;
    case GJMessageType.CMD_STOP:
        this.closeDecoder();
        break;
    case GJMessageType.CMD_PAUSE:
        this.pause(req.streamId);
        break;
    case GJMessageType.CMD_RESUME:
        this.resume(req.streamId);
        break;
    case GJMessageType.CMD_VOLUME:
        this.volume(req.streamId, req.data.volume);
        break;
    case GJMessageType.CMD_SNAPSHOT:
        this.snapshot(req.streamId);
        break;
    case GJMessageType.CMD_PLAYBACK:
        this.playback(req.streamId);
        break;
    case GJMessageType.CMD_UNPLAYBACK:
        this.unplayback(req.streamId);
        break;
    case GJMessageType.CMD_MUTE:
        console.log(req);
        this.mute(req.streamId, req.data.isMute);
        break;
    default:
        throw new Error("Decoder error Type");
    }
};

Decoder.prototype.cacheReq = function (req) {
    if (req) {
        if (req.type === GJMessageType.CMD_DECODER_OPEN) {
            this.tmpReqQue.push(req);
        }
    }
};

Decoder.prototype.onWasmLoaded = function () {
    console.log("Wasm loaded.");
    this.wasmLoaded = true;
    this.cacheBuffer = Module._malloc(1024 * 1024); //申请1M空间传输视频数据
    this.setCallback();
    while (this.tmpReqQue.length > 0) {
        var req = this.tmpReqQue.shift();
        this.processReq(req);
    }
};

self.decoder = new Decoder;

self.onmessage = function (evt) {
    if (!self.decoder) {
        console.log("[ER] Decoder not initialized!");
        return;
    }

    var req = evt.data;

    if (req.type !== GJMessageType.ON_PACKET) {
        console.log("decoder onmessage >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        console.log(req);
        console.log("decoder onmessage <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    }

    if (!self.decoder.wasmLoaded) {
        self.decoder.cacheReq(req);
        console.log("Temp cache req " + req.type + ".");
        return;
    }
    self.decoder.processReq(req);
};

function onWasmLoaded() {
    if (self.decoder) {
        self.decoder.onWasmLoaded();
    } else {
        console.log("[ER] No decoder!");
    }
}
