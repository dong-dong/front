/*
webWorker：
一个浏览器对象，只包含四个属性：appName,appVersion,userAgent,platform
一个location对象（和window里的一样，但是里面所有的属性是只读的）
一个self对象指向全局Web Worker线程对象
一个importScripts()方法使Web Worker能够加载外部js文件
所有的ECMAScript对象
XMLHttpRequest构造器
setTimeout()和setInterval()方法
*/

self.importScripts("common.js");

function PlayerMgr() {
    let _this = this;
    let playerList={};
    let s_genDecoderStreamId = 1;;
    this.getNewDecoderStreamId = function(){
        let ret = s_genDecoderStreamId++;
        if(s_genDecoderStreamId > 99999) s_genDecoderStreamId = 1;
        return ret;
    };
    this.setPlayerTimestamp = function(playerId){
        if(playerList[playerId]){
            playerList[playerId].timestamp = new Date().getTime();
        }
    }
    this.setPlayerReceiverStreamId = function(playerId,streamId){
        if(playerList[playerId]){
            playerList[playerId].receiverStreamId = streamId;
        }
    }
    this.getPlayerReceiverStreamId = function(playerId){
        if(playerList[playerId]){
            return playerList[playerId].receiverStreamId;
        }
        return -1;
    }
    this.getPlayerDecoderStreamId = function(playerId){
        if(playerList[playerId]){
            return playerList[playerId].decoderStreamId;
        }
        return -1;
    }
    this.getReciverWorker = function(playerId){
        if(playerList[playerId]){
            return playerList[playerId].receiverWorker;
        }
        return undefined;
    };
    this.getDecoderWorker = function(playerId){
        if(playerList[playerId]){
            return playerList[playerId].decodeWorker;
        }
        return undefined;
    };
    this.checkTimeout = function(){
        console.log("PlayerMgr::checkTimeout():");
        let closeList = [];
        for (let index in playerList) {
            console.log(playerList[index]);
            if(playerList[index] && playerList[index].timestemp > 1570000000000){
                if(playerList[index].timestamp - (new Date().getTime()) > 60000) //60s超时
                closeList.push(index);
            }else{
                throw new Error(`[PlayerMgr.checkTimeout] timestamp error ${playerList[index].timestamp}`);
            }
        }
        for (let index = 0; index < closeList.length; index++) {
            _this.close(closeList[index]);
        }
    }
    this.clear = function (){
        let closeList = [];
        for (let index in playerList) {
            closeList.push(index);
        }
        for (let index = 0; index < closeList.length; index++) {
            _this.close(closeList[index]);
        }
    };
    this.play = function (req) {
        //{type,playerId,deviceId,streamId,streamId}
        console.log("PlayerMgr::play():");
        _this.checkTimeout()
        let playerId = req.playerId;
        let decoderStreamId = _this.getNewDecoderStreamId();
        let playerInfo = {
            playerId:playerId,
            timestemp:new Date().getTime(),
            deviceId:req.data.deviceId,
            deviceStreamId:req.data.streamId,
            decoderStreamId:decoderStreamId,
            receiverStreamId:-1
        };
        playerInfo.decodeWorker = new Worker("decoder.js");
        playerInfo.decodeWorker.onmessage = function (evt) {
            let res = evt.data;
            if(res.type != GJMessageType.ON_VIDEO_FRAME && res.type != GJMessageType.ON_AUDIO_FRAME){
                console.log("decodeWorker onmessage >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                console.log(res);
                console.log("decodeWorker onmessage <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
            }
            switch (res.type) {
                case GJMessageType.CMD_DECODER_OPEN:
                    console.log(`[${playerId}] Open Decoder`);
                    break;
                case GJMessageType.ON_VIDEO_FRAME:
                case GJMessageType.ON_AUDIO_FRAME:
                case GJMessageType.ON_SNAPSHOT:
                    self.postMessage({playerId:playerId,...res},[res.data.data.buffer]);
                    break;
                default:
                    throw new Error("[DecodeWorker.onmessage] req.type error ");
            }
        }
        playerInfo.receiverWorker = new Worker("receiver.js");
        playerInfo.receiverWorker.onmessage = function (evt) {
            let res = evt.data;
            if(res.type != GJMessageType.ON_PACKET){
                console.log("receiverWorker onmessage >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                console.log(res);
                console.log("receiverWorker onmessage <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
            }
            switch (res.type) {
                case GJMessageType.ON_PACKET:
                    if(res.packetType === GJPacketType.NONE){
                        // not to do anything
                    } else if(res.packetType === GJPacketType.INFO){
                        let info = {};
                        try {
                            info = JSON.parse(res.message);
                        } catch (error) {
                            console.log(error);
                            throw new Error(error);
                        }
                        _this.setPlayerReceiverStreamId(playerId,info.streamId);
                        self.postMessage({type:GJMessageType.ON_INFO,playerId:playerId,data:info});
                        let openDecoderReq = {
                            type:GJMessageType.CMD_DECODER_OPEN,
                            streamId:decoderStreamId,
                            message:res.message
                        };
                        playerInfo.decodeWorker.postMessage(openDecoderReq);
                    }else{
                        let onPacketReq = {
                            type:GJMessageType.ON_PACKET,
                            streamId:decoderStreamId,
                            packetType:res.packetType,
                            payload:res.payload,
                            timestamp:res.timestamp
                        };
                        playerInfo.decodeWorker.postMessage(onPacketReq,[onPacketReq.payload.buffer]);
                    }
                    break;
                default:
                    throw new Error("[DecodeWorker.onmessage] req.type error ");
            }
        }
        playerInfo.receiverWorker.postMessage(req);
        playerList[playerId] = playerInfo;
        let objData = {
            playerId:playerId,
            type:GJMessageType.CMD_PLAY
        };
        self.postMessage(objData);
    };
    
    this.ack = function (req) {
        _this.setPlayerTimestamp(req.playerId);
        self.postMessage(req);
    };
    
    this.close = function (playerId) {
        if(playerList[playerId]){
            if(playerList[playerId].receiverWorker){
                //TODO 关闭接收器
                playerList[playerId].receiverWorker.terminate();
            }
            if(playerList[playerId].decodeWorker){
                //TODO 关闭解码器
                playerList[playerId].decodeWorker.terminate();
            }
            delete playerList[playerId];
        }
        let objData = {
            playerId:playerId,
            type:GJMessageType.CMD_STOP
        };
        self.postMessage(objData);
    };
    this.operation = function(req){
        let decodeWorker = _this.getDecoderWorker(req.playerId);
        req.streamId = _this.getPlayerDecoderStreamId(req.playerId);
        if(decodeWorker && req.streamId != -1) decodeWorker.postMessage(req);
        self.postMessage(req);
    };
    this.processReq = function (req) {
        switch (req.type) {
            case GJMessageType.CMD_CLEAR:
                _this.clear();
                break;
            case GJMessageType.CMD_TERMINATE:
                _this.clear();
                self.terminate()
                break;
            case GJMessageType.CMD_PLAY:
                _this.play(req);
                break;
            case GJMessageType.CMD_ACK:
                _this.ack(req);
                break;
            case GJMessageType.CMD_STOP:
                _this.close(req.playerId);
                break
            case GJMessageType.CMD_PAUSE:
            case GJMessageType.CMD_RESUME:
            case GJMessageType.CMD_VOLUME:
            case GJMessageType.CMD_PLAYBACK:
            case GJMessageType.CMD_UNPLAYBACK:
            case GJMessageType.CMD_SNAPSHOT:
            case GJMessageType.CMD_MUTE:
                _this.operation(req)
                break;
            default:
                throw new Error("[playerMgr.processReq] req.type error ");
        }
    };
}


self.playerMgr = new PlayerMgr();
self.onmessage = function (evt) {
    if (!self.playerMgr) {
        throw new Error("[playerMgr.onmessage] playerMgr initialized!");
    }

    var req = evt.data;
    console.log("playerMgr onmessage >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    console.log(req);
    console.log("playerMgr onmessage <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    self.playerMgr.processReq(req);
};
function check(){
    self.playerMgr.checkTimeout();
};
// self.interval = setInterval(check,10000);


