outdatedBrowserRework(
    {
        fullscreen: false,
        browserSupport: {
            'Chrome': 69,
            'Edge': 39,
            'Safari': false,
            'Mobile Safari': false,
            'Firefox': 62,
            'Opera': false,
            'IE': false
        },
        requireChromeOnAndroid: false,
        isUnknownBrowserOK: true,
        messages: {
            cn: {
                "outOfDate": "当前浏览器版本不支持系统正常运行！",
                "update": {
                    "web": "要正常浏览本网站请联系管理员安装 Chrome / Firefox 浏览器。",
                },
                "url": "",
                "callToAction": "",
                "close": "关闭"
            }
        },
        language: 'cn'
    }
);
