(function (window) {

    /* Change this variable to update the environment variable value */
    const environmentVariable = {
        client: 'gateway-dev-tools',
        apiUrl: 'http://10.0.20.51:3117/graphql',
        wsUrl: 'ws://10.0.20.51:3117/graphql',
        domainId: 1
    };

    window.__env = window.__env || {};

    window.__env = environmentVariable;

}(this));
