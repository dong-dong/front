#### version 2019-10-23 10:06:30

GJMessageType 通讯样式
===============================================================
```
const GJMessageType = {
    //PalyerMgr
    CMD_CLEAR:1,
    CMD_TERMINATE:2,
    //PLAYER
    CMD_PLAY:101,//OK
    CMD_ACK:102,//OK
    CMD_STOP:103,//OK
    CMD_PAUSE:104,//OK
    CMD_RESUME:105,//OK
    CMD_VOLUME:106,//OK
    CMD_PLAYBACK:107,//OK
    CMD_SNAPSHOT:108,//OK
    CMD_MUTE:109,//OK
    CMD_UNPLAYBACK:110,//OK
    //RECEVICER
    CMD_RECEVICER_START:201,
    CMD_RECEVICER_STOP:202,
    //DECODER
    CMD_DECODER_OPEN:301,
    CMD_DECODER_CLOST:302,
    //EVENT
    ON_INFO:501,
    ON_PACKET:502,
    ON_VIDEO_FRAME:503,
    ON_AUDIO_FRAME:504,
    //NONE
    NONE:0
}
```
worker间通讯样式

0~99为播放管理器（palyerMgr）指令

100~199为播放器（player）指令

500～为事件

play播放指令(CMD_PLAY:101)
===============================================================
```
{
  "type": 101,
  "playerId": 1,
  "data": {
    "clientId": 1,
    "token": "token",
    "url": "ws://10.0.0.213:9090/",
    "deviceId": 3
    "streamId":0;
  }
}
```
需要播放器状态为未使用


playerId的字段作为key使用，样式number或者string均可，
由于webWorker的可跨窗口使用的属性,
playerId建议使用UUID等生成函数生成，
尽量保证浏览器程序中的唯一性。


clientId:余留鉴权字段
token：余留鉴权字段
deviceStreamId: 
  0：主码流；
  1：子码流1；
  2：子码流2；

ack确认保活指令（CMD_ACK:102)
===============================================================
```
{"type":102,"playerId":1}
```
需要播放器状态为使用中

需要定时执行次指令

目前超时时间60s，检测10s一次，播放器建议每30秒一次保活命令，

在实际部署中建议使用可配置的参数。

stop关闭指令(CMD_STOP:103)
===============================================================
```
{"type":103,"playerId":1}
```
需要播放器状态为使用中

pause暂停指令(CMD_PAUSE:104)
===============================================================
```
{"type":104,"playerId":1}
```
需要播放器状态为播放中

resume恢复播放指令(CMD_RESUME:105)
===============================================================
```
{"type":102,"playerId":1}
```
需要播放器状态为暂停中

volume音量调节指令(CMD_VOLUME:106) 过时不建议使用
===============================================================
```
{
  "type": 102,
  "playerId": 1，
  "data": {
    "volume": 50
  }
}
```
需要播放器状态为使用中

volume的样式为number，需要为0~100中的整形数值

静音通过volume：0实现（如果volume:0解码器直接抛弃音频帧）

所有播放器默认状态volume为0；

Mute静音指令(CMD_MUTE:109)
===============================================================
```
{
  "type": 102,
  "playerId": 1，"data": {
    "isMute": true
  }
}
```
isMute: 布尔类型,设置是否开启静音(默认静音);

回放指令(CMD_PLAYBACK:107)
===============================================================
```
{
  "type": 107,
  "playerId": 1
}
```
回放中目前无法暂停与恢复,如果有需求,可增加相应功能.
回放结束画面因没有更多的数据停止,需要取消回放返回实时播放.


取消回放指令(CMD_UNPLAYBACK:110)
===============================================================
```
{
  "type": 110,
  "playerId": 1
}
```
取消回放后,目前会直接转为实时播放,如果开始播放时为暂停状态,取消回放会自动开始实时播放.(注意界面按钮状态更新)
目前返回实时播放时有短暂花屏问题为正常现象,下阶段工作处理.

info播放器信息事件(ON_INFO:501)
===============================================================
```
{
  "type": 501,
  "playerId": 1,
  data: {
    "video": {
      "codec_id": 28,
      "rate": 90000,
      "pix_fmt": 0,
      "width": 1280,
      "height": 720,
      "extradata": "0000000167640028ace805005b900000000168ee3cb0",
      "extradata_size": 22
    },
    "audio": {
      "codec_id": 86018,
      "sample_fmt": 8,
      "channels": 1,
      "sample_rate": 16000,
      "rate": 16000,
      "extradata": "1408",
      "extradata_size": 2
    }
  }
}
```
次信息用户初始化视频YUV播放器和音频pcm播放器，（信息有有富裕，如果有必要可以裁剪）

YUV视频数据事件(ON_VIDEO_FRAME:503)
===============================================================
```
{
  playerId: 1,
  type: 503,
  data: {
    streamId: 1,
    timestamp: 8079.807000000001,
    data: Uint8Array(1382400)
  }
}
```
timestamp样式float,单位秒,相对时间戳,摄像头设备被请求数据时开始的时间戳,

注意:由于服务器与摄像头之间可能存在断线重连,timestamp可能被重置,目前界面无时间戳显示,暂时可以忽略

streamId对于播放器层无意义

视频YUV数据

PCM音频数据事件(ON_AUDIO_FRAME:504)
===============================================================
```
{
  playerId: 1,
  type: 504,
  data: {
    streamId: 1,
    timestamp: 8079.817,
    data: Uint8Array(4096)
  }
}
```
timestamp样式float,单位秒,相对时间戳,摄像头设备被请求数据时开始的时间戳,

注意:由于服务器与摄像头之间可能存在断线重连,timestamp可能被重置,目前界面无时间戳显示,暂时可以忽略

streamId对于播放器层无意义

音频PCM数据

备注
===============================================================
部分type为子worker之间通讯使用，未在文档中体现