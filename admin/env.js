(function (window) {

    /* Change this variable to update the environment variable value */
    const environmentVariable = {
        client: 'gateway-admin',
        apiUrl: 'http://10.0.20.51:3129/graphql',
        // apiUrl: 'http://10.0.0.38:3005/graphql',
        wsUrl: 'ws://10.0.20.51:3129/graphql',
        uploadUrl: 'http://10.0.1.212:8888',
        domainId: 1,
        loginUrl: '/login',
        accountErrorCodeArray: [],
        ngForageName: 'WEB_OPEN_PLATFORM_ADMIN'
    };

    window.__env = window.__env || {};

    window.__env = environmentVariable;

}(this));
